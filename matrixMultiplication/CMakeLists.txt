cmake_minimum_required(VERSION 3.16)
project(matrix_multiplication)

add_executable(${PROJECT_NAME} main.cpp)
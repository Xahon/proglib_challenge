#include <cassert>
#include <iostream>
#include <memory>
#include <algorithm>

// clang-format off
static const int matrix1[16] = {
  1,  2,  3,  4,
  5,  6,  7,  8,
  9,  10, 11, 12,
  13, 14, 15, 16,
};

static const int matrix2[16] = {
  7, 8, 9, 4,
  5, 6, 1, 2,
  3, 0, 7, 8,
  9, 4, 5, 6,
};

static const int expected[16] = {
   62,  36,  52,  56,
  158, 108, 140, 136,
  254, 180, 228, 216,
  350, 252, 316, 296,
};
// clang-format on

template <typename T>
T &access(T *matrix, size_t row, size_t col, size_t dimensions) {
  return matrix[col + row * dimensions];
}

template <typename T>
void multiply_square_matrices(const T *matrix1, const T *matrix2, T *out, size_t dimensions) {
  for (int row = 0; row < dimensions; ++row) {
    for (int col = 0; col < dimensions; ++col) {
        T value = 0;

        for (int k = 0; k < dimensions; ++k) {
          value += (access(matrix1, row, k, 4) * access(matrix2, k, col, 4));
        }

        access(out, row, col, 4) = value;
    }
  }
}

int main() {
  std::cout << "Matrix Multiplication" << std::endl;

  std::cout << "Matrix 1:" << std::endl;
  for (auto row = 0; row < 4; ++row) {
    for (auto col = 0; col < 4; ++col) {
      std::cout << access(matrix1, row, col, 4) << "\t";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;

  std::cout << "Matrix 2:" << std::endl;
  for (auto row = 0; row < 4; ++row) {
    for (auto col = 0; col < 4; ++col) {
      std::cout << access(matrix2, row, col, 4) << "\t";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;

  std::cout << "Expected:" << std::endl;
  for (auto row = 0; row < 4; ++row) {
    for (auto col = 0; col < 4; ++col) {
      std::cout << access(expected, row, col, 4) << "\t";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;

  int new_matrix[16];
  multiply_square_matrices(matrix1, matrix2, new_matrix, 4);

  std::cout << "Computed:" << std::endl;
  for (auto row = 0; row < 4; ++row) {
    for (auto col = 0; col < 4; ++col) {
      std::cout << access(new_matrix, row, col, 4) << "\t";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;

  assert(new_matrix[0] == expected[0]);
  assert(new_matrix[1] == expected[1]);
  assert(new_matrix[2] == expected[2]);
  assert(new_matrix[3] == expected[3]);
  assert(new_matrix[4] == expected[4]);
  assert(new_matrix[5] == expected[5]);
  assert(new_matrix[6] == expected[6]);
  assert(new_matrix[7] == expected[7]);
  assert(new_matrix[8] == expected[8]);
  assert(new_matrix[9] == expected[9]);
  assert(new_matrix[10] == expected[10]);
  assert(new_matrix[11] == expected[11]);
  assert(new_matrix[12] == expected[12]);
  assert(new_matrix[13] == expected[13]);
  assert(new_matrix[14] == expected[14]);
  assert(new_matrix[15] == expected[15]);

  std::cout << "Matrix Multiplication - OK" << std::endl;
  return 0;
}
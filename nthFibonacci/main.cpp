#include <cassert>
#include <iostream>
#include <memory>
#include <algorithm>
#include <vector>

static const std::vector<int> nths = {0, 1, 2, 3, 4, 10, 46};

struct matrix2x2 {
  int v[2][2] = {{0, 0}, {0, 0}};

  matrix2x2 &pow(int power) {
    matrix2x2 init = *this;
    for (int i = 1; i < power; ++i) {
      (*this) *= init;
    }
    return *this;
  }

  matrix2x2 operator*(const matrix2x2 &other) const {
    matrix2x2 t = *this;
    t *= other;
    return t;
  }

  matrix2x2 operator*=(const matrix2x2 &other) {
    auto &m1 = *this;
    const auto &m2 = other;
    int a11 = m1.v[0][0] * m2.v[0][0] + m1.v[0][1] * m2.v[1][0];
    int a12 = m1.v[0][0] * m2.v[0][1] + m1.v[0][1] * m2.v[1][1];
    int a21 = m1.v[1][0] * m2.v[0][0] + m1.v[1][1] * m2.v[1][0];
    int a22 = m1.v[1][0] * m2.v[0][1] + m1.v[1][1] * m2.v[1][1];
    m1.v[0][0] = a11;
    m1.v[0][1] = a12;
    m1.v[1][0] = a21;
    m1.v[1][1] = a22;
    return m1;
  }
};

int nth_fibo_iter(int nth) {
  assert(nth >= 0);
  if (nth == 0) {
    return 0;
  }

  int prev = 0;
  int cur = 1;
  int num = 1;
  for (int i = 0; i < nth - 1; ++i) {
    num = prev + cur;
    prev = cur;
    cur = num;
  }
  return num;
}

int nth_fibo_rec(int nth) {
  if (nth < 2)
    return nth;
  return nth_fibo_rec(nth - 1) + nth_fibo_rec(nth - 2);
}

int nth_fibo_mat(int nth) {
  assert(nth >= 0);
  if (nth == 0) {
    return 0;
  }
  matrix2x2 mat;
  mat.v[0][0] = 1;
  mat.v[0][1] = 1;
  mat.v[1][0] = 1;
  mat.v[1][1] = 0;
  mat.pow(nth - 1);
  return mat.v[0][0];
}

int main() {
  std::cout << "N-th Fibonacci Iterative Approach" << std::endl;
  for (auto it = nths.begin(); it != nths.end(); ++it) {
    int number = nth_fibo_iter(*it);
    std::cout << *it << "-th value " << number << std::endl;
  }
  std::cout << "N-th Fibonacci Iterative Approach - OK" << std::endl << std::endl;

  std::cout << "N-th Fibonacci Recursive Approach" << std::endl;
  for (auto it = nths.begin(); it != nths.end(); ++it) {
    if (*it > 10) {
      std::cout << *it << "-th value omitted" << std::endl;
      continue;
    }
    int number = nth_fibo_rec(*it);
    std::cout << *it << "-th value " << number << std::endl;
  }
  std::cout << "N-th Fibonacci Recursive Approach - OK" << std::endl << std::endl;

  std::cout << "N-th Fibonacci Matrix Approach" << std::endl;
  for (auto it = nths.begin(); it != nths.end(); ++it) {
    int number = nth_fibo_mat(*it);
    std::cout << *it << "-th value " << number << std::endl;
  }
  std::cout << "N-th Fibonacci Matrix Approach - OK" << std::endl;

  return 0;
}
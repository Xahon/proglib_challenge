#include <cassert>
#include <cmath>
#include <iostream>
#include <memory>
#include <algorithm>
#include <vector>

static const std::vector<std::tuple<int, int, int>> pairs = {
std::make_tuple(54, 24, 6),
std::make_tuple(42, 56, 14),
std::make_tuple(62, 36, 2),
};

int gcd(int n1, int n2) {
  if (n1 == 0 || n2 == 0) {
    return 0;
  }

  int g = 0;
  int d = 0;

  while (n1 % 2 == 0 && n2 % 2 == 0) {
    n1 /= 2;
    n2 /= 2;
    ++d;
  }

  while (n1 != n2) {
    if (n1 % 2 == 0) {
      n1 /= 2;
    } else if (n2 % 2 == 0) {
      n2 /= 2;
    } else if (n1 > n2) {
      n1 = (n1 - n2) / 2;
    } else {
      n2 = (n2 - n1) / 2;
    }
  }
  g = n1;
  return g * pow(2, d);
}

int main() {
  std::cout << "Greatest Common Divisor" << std::endl;

  for (auto it = pairs.begin(); it != pairs.end(); ++it) {
    int n1 = std::get<0>(*it);
    int n2 = std::get<1>(*it);
    int n3 = std::get<2>(*it);
    auto g = gcd(n1, n2);
    std::cout << "GCD of " << n1 << " " << n2 << ", expected " << n3 << ", given " << g << std::endl;
    assert(g == n3);
  }

  std::cout << "Greatest Common Divisor - OK" << std::endl;
  return 0;
}
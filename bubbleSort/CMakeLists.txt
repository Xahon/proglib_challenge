cmake_minimum_required(VERSION 3.16)
project(bubble_sort)

add_executable(${PROJECT_NAME} main.cpp)
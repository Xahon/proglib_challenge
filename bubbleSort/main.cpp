#include <cassert>
#include <iostream>
#include <algorithm>

static const int unordered[10] = {9, 6, 3, 7, 8, 2, 1, 5, 8, 3};

template <typename T>
void bubble_sort(T *array, size_t size) {
  if (size <= 1) {
    return;
  }

  bool has_swap;
  do {
    has_swap = false;
    for (int i = 0; i < size - 1; ++i) {
      for (int j = i + 1; j < size; ++j) {
        if (array[i] > array[j]) {
          std::swap(array[i], array[j]);
          has_swap = true;
          break;
        }
      }
    }
  } while (has_swap);
}

int main() {
  std::cout << "Bubble Sort" << std::endl;

  int *ordered;
  std::copy_n(unordered, 10, ordered);
  bubble_sort(ordered, 10);

  std::cout << "Unordered:" << std::endl;
  for (auto i = 0; i < 10; ++i)
    std::cout << unordered[i] << ", ";
  std::cout << std::endl;

  std::cout << "Ordered:" << std::endl;
  for (auto i = 0; i < 10; ++i)
    std::cout << ordered[i] << ", ";
  std::cout << std::endl;

  assert(ordered[0] == 1);
  assert(ordered[1] == 2);
  assert(ordered[2] == 3);
  assert(ordered[3] == 3);
  assert(ordered[4] == 5);
  assert(ordered[5] == 6);
  assert(ordered[6] == 7);
  assert(ordered[7] == 8);
  assert(ordered[8] == 8);
  assert(ordered[9] == 9);

  std::cout << "Bubble Sort - OK" << std::endl;
  return 0;
}
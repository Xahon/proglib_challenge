#include <cassert>
#include <iostream>
#include <vector>

void sieve_by_eratosthenes(std::vector<int> &list) {
  std::vector<int>::iterator it;
  for (int i = 0; list.begin() + i != list.end(); ++i) {
    it = list.begin() + i;
    for (auto inner_it = it + 1; inner_it != list.end();) {
      if ((*inner_it) % (*it) == 0) {
        inner_it = list.erase(inner_it);
      } else {
        ++inner_it;
      }
    }
  }
}

int main() {
  std::vector<int> list;
  for (int i = 2; i <= 100; ++i) {
    list.push_back(i);
  }

  sieve_by_eratosthenes(list);

  for (auto it = list.begin(); it != list.end(); ++it)
    std::cout << *it << ", ";
  std::cout << std::endl;

  int index = -1;
  assert(list.at(++index) == 2);
  assert(list.at(++index) == 3);
  assert(list.at(++index) == 5);
  assert(list.at(++index) == 7);
  assert(list.at(++index) == 11);
  assert(list.at(++index) == 13);
  assert(list.at(++index) == 17);
  assert(list.at(++index) == 19);
  assert(list.at(++index) == 23);
  assert(list.at(++index) == 29);
  assert(list.at(++index) == 31);
  assert(list.at(++index) == 37);
  assert(list.at(++index) == 41);
  assert(list.at(++index) == 43);
  assert(list.at(++index) == 47);
  assert(list.at(++index) == 53);
  assert(list.at(++index) == 59);
  assert(list.at(++index) == 61);
  assert(list.at(++index) == 67);
  assert(list.at(++index) == 71);
  assert(list.at(++index) == 73);
  assert(list.at(++index) == 79);
  assert(list.at(++index) == 83);
  assert(list.at(++index) == 89);
  assert(list.at(++index) == 97);
  
  return 0;
}
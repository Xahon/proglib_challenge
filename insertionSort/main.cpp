#include <cassert>
#include <iostream>
#include <algorithm>

static const int unordered[10] = {9, 6, 3, 7, 8, 2, 1, 5, 8, 3};

template <typename T>
void insertion_sort(T *array, size_t size) {
  int ordered_count = 1;
  for (int i = ordered_count; i < size; ++i) {
    const int &current_element = array[i];
    if (current_element > array[ordered_count - 1]) {
      // this element greater that the last one in already ordered part
      ++ordered_count;
      continue;
    }
    
    for (int j = 0; j < ordered_count; ++j) {
      const int& ordered_element = array[j];
      if (current_element < ordered_element) {
        int original = current_element;
        for (int k = ordered_count; k > j; --k) {
          array[k] = array[k - 1];
        }
        array[j] = original;
        ++ordered_count;
      }
    }
  }
}

int main() {
  std::cout << "Insertion Sort" << std::endl;

  int *ordered;
  std::copy_n(unordered, 10, ordered);
  insertion_sort(ordered, 10);

  std::cout << "Unordered:" << std::endl;
  for (auto i = 0; i < 10; ++i)
    std::cout << unordered[i] << ", ";
  std::cout << std::endl;

  std::cout << "Ordered:" << std::endl;
  for (auto i = 0; i < 10; ++i)
    std::cout << ordered[i] << ", ";
  std::cout << std::endl;

  assert(ordered[0] == 1);
  assert(ordered[1] == 2);
  assert(ordered[2] == 3);
  assert(ordered[3] == 3);
  assert(ordered[4] == 5);
  assert(ordered[5] == 6);
  assert(ordered[6] == 7);
  assert(ordered[7] == 8);
  assert(ordered[8] == 8);
  assert(ordered[9] == 9);

  std::cout << "Insertion Sort - OK" << std::endl;
  return 0;
}
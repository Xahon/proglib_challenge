#include <cassert>
#include <iostream>
#include <memory>
#include <algorithm>

static const int unordered[10] = {9, 6, 3, 7, 8, 2, 1, 5, 8, 3};

template <typename T>
void merge_sort(T *array, size_t size) {
  if (size <= 1) {
    return;
  }

  size_t lhs_size = size / 2;
  size_t rhs_size = size - lhs_size;
  T *lhs_ptr = array;
  T *rhs_ptr = array + lhs_size;
  merge_sort<T>(lhs_ptr, lhs_size);
  merge_sort<T>(rhs_ptr, rhs_size);

  T tmp_arr[size];

  size_t lhs_i = 0, rhs_i = lhs_size, idx = 0;
  for (int i = 0; i < size; ++i, ++idx) {
    if (lhs_i <= lhs_size && rhs_i <= size) {
      if (array[lhs_i] < array[rhs_i]) {
        tmp_arr[idx] = std::move(array[lhs_i]);
        ++lhs_i;
      } else {
        tmp_arr[idx] = std::move(array[rhs_i]);
        ++rhs_i;
      }
    }

    if (lhs_i == lhs_size) {
      std::move(&array[rhs_i], &array[size], &tmp_arr[++idx]);
      break;
    }

    if (rhs_i == size) {
      std::move(&array[lhs_i], &array[lhs_size], &tmp_arr[++idx]);
      break;
    }
  }

  std::move(tmp_arr, tmp_arr + size, array);
}

int main() {
  std::cout << "Merge Sort" << std::endl;

  int *ordered;
  std::copy_n(unordered, 10, ordered);
  merge_sort(ordered, 10);

  std::cout << "Unordered:" << std::endl;
  for (auto i = 0; i < 10; ++i)
    std::cout << unordered[i] << ", ";
  std::cout << std::endl;

  std::cout << "Ordered:" << std::endl;
  for (auto i = 0; i < 10; ++i)
    std::cout << ordered[i] << ", ";
  std::cout << std::endl;

  assert(ordered[0] == 1);
  assert(ordered[1] == 2);
  assert(ordered[2] == 3);
  assert(ordered[3] == 3);
  assert(ordered[4] == 5);
  assert(ordered[5] == 6);
  assert(ordered[6] == 7);
  assert(ordered[7] == 8);
  assert(ordered[8] == 8);
  assert(ordered[9] == 9);

  std::cout << "Merge Sort - OK" << std::endl;
  return 0;
}
#include <cassert>
#include <iostream>
#include <memory>
#include <algorithm>

static const int unordered[10] = {9, 6, 3, 7, 8, 2, 1, 5, 8, 3};

template <typename T>
int partition(T *array, int low, int high) {
  int pivot = array[(low + high) / 2];
  int lhs_i = low - 1, rhs_i = high + 1;

  if (high - low == 1) {
    if (array[low] > array[high]) {
      std::swap(array[low], array[high]);
    }
    return low;
  }

  while (true) {
    do {
      ++lhs_i;
    } while (array[lhs_i] < pivot);

    do {
      --rhs_i;
    } while (array[rhs_i] > pivot);

    if (lhs_i >= rhs_i) {
      return rhs_i;
    }
    std::swap(array[lhs_i], array[rhs_i]);
  }
}

template <typename T>
void quick_sort(T *array, int low, int high) {
  if (low < high) {
    int pivot_idx = partition<T>(array, low, high);
    quick_sort<T>(array, low, pivot_idx);
    quick_sort<T>(array, pivot_idx + 1, high);
  }
}

template <typename T>
void quick_sort(T *array, size_t size) {
  quick_sort(array, 0, size - 1);
}

int main() {
  std::cout << "Quick Sort" << std::endl;

  int *ordered;
  std::copy_n(unordered, 10, ordered);
  quick_sort(ordered, 10);

  std::cout << "Unordered:" << std::endl;
  for (auto i = 0; i < 10; ++i)
    std::cout << unordered[i] << ", ";
  std::cout << std::endl;

  std::cout << "Ordered:" << std::endl;
  for (auto i = 0; i < 10; ++i)
    std::cout << ordered[i] << ", ";
  std::cout << std::endl;

  assert(ordered[0] == 1);
  assert(ordered[1] == 2);
  assert(ordered[2] == 3);
  assert(ordered[3] == 3);
  assert(ordered[4] == 5);
  assert(ordered[5] == 6);
  assert(ordered[6] == 7);
  assert(ordered[7] == 8);
  assert(ordered[8] == 8);
  assert(ordered[9] == 9);

  std::cout << "Quick Sort - OK" << std::endl;
  return 0;
}